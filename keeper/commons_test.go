// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"testing"

	"github.com/cosmos/cosmos-sdk/baseapp"
	"github.com/cosmos/cosmos-sdk/codec"
	codecTypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/store"
	storeTypes "github.com/cosmos/cosmos-sdk/store/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tendermint/tendermint/libs/log"
	tendermintTypes "github.com/tendermint/tendermint/proto/tendermint/types"
	tendermintTmDb "github.com/tendermint/tm-db"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
)

type TokenTestSuite struct {
	suite.Suite
	keeper      keeper.Keeper
	app         *app.App
	ctx         sdk.Context
	queryClient types.QueryClient
	addr1       sdk.AccAddress
	addr2       sdk.AccAddress
}

func (suite *TokenTestSuite) SetupTest() {
	testableApp, ctx := app.CreateTestInput()

	suite.keeper = testableApp.HashtokenKeeper
	suite.app = testableApp
	suite.ctx = ctx

	creator1, _ := sdk.AccAddressFromBech32(app.CreatorA)
	creator2, _ := sdk.AccAddressFromBech32("cosmos13ewe06vu6255fqrmhz2gqyhqjvh6rxw837sc30")

	suite.addr1 = creator1
	suite.addr2 = creator2

	querier := keeper.Querier{Keeper: testableApp.HashtokenKeeper}
	queryHelper := baseapp.NewQueryServerTestHelper(ctx, testableApp.InterfaceRegistry())
	types.RegisterQueryServer(queryHelper, querier)

	suite.queryClient = types.NewQueryClient(queryHelper)
}

func (suite *TokenTestSuite) SetupToken() string {
	msgCreateToken := types.MsgCreateToken{
		Creator:       suite.addr1.String(),
		Id:            app.TokenId1,
		Timestamp:     app.Timestamp,
		TokenType:     app.TokenType,
		ChangeMessage: app.NotChanged,
		SegmentId:     app.SegmentId1,
		Document:      app.Document,
		Hash:          app.Hash,
		HashFunction:  app.HashFunction,
		Metadata:      app.Metadata,
	}

	info := types.Info{
		Document:     msgCreateToken.Document,
		Hash:         msgCreateToken.Hash,
		HashFunction: msgCreateToken.HashFunction,
		Metadata:     msgCreateToken.Metadata,
	}

	id := suite.keeper.AppendToken(
		suite.ctx,
		msgCreateToken.Creator,
		msgCreateToken.Id,
		msgCreateToken.Timestamp,
		msgCreateToken.TokenType,
		msgCreateToken.ChangeMessage,
		msgCreateToken.SegmentId,
		info,
	)
	return id
}

func setupKeeper(t testing.TB) (*keeper.Keeper, sdk.Context) {
	storeKey := sdk.NewKVStoreKey(types.StoreKey)
	memStoreKey := storeTypes.NewMemoryStoreKey(types.MemStoreKey)

	db := tendermintTmDb.NewMemDB()
	stateStore := store.NewCommitMultiStore(db)
	stateStore.MountStoreWithDB(storeKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(memStoreKey, sdk.StoreTypeMemory, nil)
	require.NoError(t, stateStore.LoadLatestVersion())

	registry := codecTypes.NewInterfaceRegistry()
	newKeeper := keeper.NewKeeper(
		codec.NewProtoCodec(registry),
		storeKey,
		memStoreKey,
	)

	ctx := sdk.NewContext(stateStore, tendermintTypes.Header{}, false, log.NewNopLogger())
	return newKeeper, ctx
}
