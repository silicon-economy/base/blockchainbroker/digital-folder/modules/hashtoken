// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	cdctypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

func RegisterCodec(cdc *codec.LegacyAmino) {
	// this line is used by starport scaffolding # 2
	cdc.RegisterConcrete(&MsgFetchAllToken{}, "HashToken/FetchAllToken", nil)

	cdc.RegisterConcrete(&MsgFetchAllTokenHistory{}, "HashToken/FetchAllTokenHistory", nil)

	cdc.RegisterConcrete(&MsgFetchTokenHistory{}, "HashToken/FetchTokenHistory", nil)

	cdc.RegisterConcrete(&MsgFetchToken{}, "HashToken/FetchToken", nil)

	cdc.RegisterConcrete(&MsgCreateTokenHistory{}, "HashToken/CreateTokenHistory", nil)
	cdc.RegisterConcrete(&MsgUpdateTokenHistory{}, "HashToken/UpdateTokenHistory", nil)

	cdc.RegisterConcrete(&MsgCreateToken{}, "HashToken/CreateToken", nil)
	cdc.RegisterConcrete(&MsgUpdateToken{}, "HashToken/UpdateToken", nil)

	cdc.RegisterConcrete(&MsgActivateToken{}, "HashToken/ActivateToken", nil)
	cdc.RegisterConcrete(&MsgDeactivateToken{}, "HashToken/DeactivateToken", nil)

	cdc.RegisterConcrete(&MsgUpdateTokenInformation{}, "HashToken/UpdateTokenInformation", nil)
}

func RegisterInterfaces(registry cdctypes.InterfaceRegistry) {
	// this line is used by starport scaffolding # 3
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateTokenHistory{},
		&MsgUpdateTokenHistory{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateToken{},
		&MsgUpdateToken{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgActivateToken{},
		&MsgDeactivateToken{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgUpdateTokenInformation{},
	)

	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)
